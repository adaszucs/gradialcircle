package com.thelsien.gradientcircleparts

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val gradialCircle = findViewById<GradialCircle>(R.id.gradial_circle)
        gradialCircle.startCircleAnimation()
    }
}
