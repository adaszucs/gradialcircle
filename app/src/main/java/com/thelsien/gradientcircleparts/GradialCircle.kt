package com.thelsien.gradientcircleparts

import android.animation.Animator
import android.animation.ArgbEvaluator
import android.animation.TimeAnimator
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.drawable.GradientDrawable
import android.util.AttributeSet
import android.view.View

class GradialCircle @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) :
    View(context, attrs, defStyleAttr) {

    private val gradientDrawable: GradientDrawable = GradientDrawable().apply {
        colors = intArrayOf(Color.BLACK, Color.TRANSPARENT)
        gradientType = GradientDrawable.RADIAL_GRADIENT
    }

    private val gradientAnimator: Animator = TimeAnimator.ofInt(0, 100).apply {
        duration = 500
        repeatMode = TimeAnimator.REVERSE
        repeatCount = TimeAnimator.INFINITE
        addUpdateListener {
            val fraction = it.animatedFraction
            val newStart = gradientEvaluator.evaluate(fraction, Color.BLACK, Color.TRANSPARENT) as Int
            gradientDrawable.colors = intArrayOf(newStart, Color.TRANSPARENT)
            invalidate()
        }
    }

    private val gradientEvaluator: ArgbEvaluator = ArgbEvaluator()

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        canvas?.also {
            gradientDrawable.setBounds(0, 0, width, height)
            gradientDrawable.gradientRadius = width.toFloat() / 2
            gradientDrawable.setGradientCenter(0.5f, 0.5f)
            gradientDrawable.setSize(width, height)
            gradientDrawable.draw(canvas)
        }
    }

    fun startCircleAnimation() {
        gradientAnimator.start()
    }
}